from django.http.response import HttpResponseRedirect
from django.shortcuts import render , redirect , reverse
from django.db.models import Q, query
from .models import Comment, Post , Category
from django.http import JsonResponse , HttpResponse
from .forms import RegisterForm
from django.views.generic import View, TemplateView
from django.core import serializers
from django.core.paginator import Paginator

# Create your views here.



def index(request):
    post = Post.objects.order_by('-date')[:4]
    all_posts = Post.objects.all()
    category = Category.objects.all()
    popular = Post.objects.filter(popular__gte = 10)
    paginator = Paginator(all_posts , 3)
    page_num = request.GET.get('page')
    page = paginator.get_page(page_num)

    return render(request, 'blog/base.html', {'post' :post , 'page': page, 'all_posts': paginator.count , 'category': category , 'popular': popular})



def search_result(request):
    category = Category.objects.all()
    query = request.GET.get('search')
    search_obj = Post.objects.filter(
        Q(title__icontains=query) | Q(description__icontains=query)
    )
    return render(request, 'blog/search_results.html', {'search_obj':search_obj, 'query':query , 'category': category })

def post_detail(request, slug):
    category = Category.objects.all()
    post=Post.objects.get(slug__iexact = slug)
    post.popular += 1
    post.save()
    return render(request, 'blog/post_detail.html', context={'post':post , 'category': category })




def category_detail(request, slug):
    category = Category.objects.get(slug__iexact = slug)
    posts = Post.objects.order_by('-date')
    return render(request, 'blog/category_detail.html' , {'category' : category , 'posts' : posts})
    
    


def register(request):
    print(request)
    if request.method =='POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect ('index')
    else:
        form = RegisterForm()
    return render(request, 'blog/register.html', {'form':form})



def leave_comment(request, slug):
    try:
        post = Post.objects.get(slug__iexact = slug)
    except:
        raise Http404("Article not found")
    if request.user.is_authenticated:
        user = request.user.username
        post.comments.create(author_name = user , comment_text = request.POST.get('comment_text'))
    else:
        post.comments.create(author_name = request.POST.get('name'), comment_text = request.POST.get('comment_text'))
        comment = Comment.objects.order_by('-date')[:2]
    return HttpResponseRedirect(reverse('post_detail_url' , args = (post.slug,)))

