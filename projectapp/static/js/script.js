$(document).ready(function(){
    $('.slider').slick({
        arrows: true,
        dots: true,
        adaptiveHeight: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 300,
        easing: 'ease',
        infinite: true,
        initialSlide:0,
        autoplay: true,
        autoplaySpeed: 3000,
        pauseOnFocus:true,
        pauseOnHover:true,
        pauseOnDotsHover: true,
        draggable: true,
        swipe: true,
        touchThreshold:5,
        touchMove: true,
        waitForAnimate: true,
        centerMode: false,
        variableWidth: false,
        rows: 1,
        slidesPerRow: 1,
        vertical: false,
        verticalSwiping: false,
        fade: false,
        // asNavFor:,""
        // responsive:[
        //     {
        //         breakpoint: 768,
        //         settings: {
        //             slidesToShow:2
        //         }
        //     }
        // ]
        // mobileFirst:
        // appendArrows:$('')
        // appendDots:$('')
        
    });
    // $('.slider').slick('setPosition');
    // $('.slider').slick('goTo', 2);
    // $('.slider').slick('slickPrev');
    // $('.slider').slick('slickNext');
    // $('.slider').slick('slickPlay');
    // $('.slider').slick('slickPause');
    // $('.slider').slick('slickAdd', '');
    // $('.slider').slick('slickRemove', 0);
    // var s = $('.slider').slick('slickGetOption', 'slidesToShow');
    // conslole.log('s');
    // $('.slider').slick('slickSetOption', 'slidesToShow' , 2);
    // $('.slider').slick('unslick');  
});


