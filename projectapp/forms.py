from django.contrib.auth.forms import UserCreationForm
from django import forms
from django.contrib.auth.models import User
from django.forms import fields
from django.forms.widgets import TextInput


class RegisterForm(UserCreationForm):
    username = forms.CharField(label = 'Username', required = True , widget = forms.TextInput(attrs={'class':'input'}))
    fullname = forms.CharField(label = 'Fullname', required = True , widget = forms.TextInput(attrs={'class':'input'}))
    password1 = forms.CharField(label = 'Password', required = True , widget = forms.PasswordInput(attrs={'class':'input'}))
    password2 = forms.CharField(label = 'Repeat Password', required = True , widget = forms.PasswordInput(attrs={'class':'input'}))
    email = forms.EmailField(label = 'Email', required = True , widget = forms.TextInput(attrs={'class':'input'}))
    
    class Meta:
        model = User

        fields = ("username", "fullname" , "email", "password1", "password2")
    
    def save(self, commit = True):
        user = super(RegisterForm, self).save(commit = False)
        user.email = self.cleaned_data["email"]
        user.username = self.cleaned_data["username"]
        user.fullname = self.cleaned_data["fullname"]
        
        if commit:
            user.save()
        return user